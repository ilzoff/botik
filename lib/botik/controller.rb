module Botik
  class Controller
    include ActiveSupport::Callbacks
    include ActiveSupport::Rescuable

    define_callbacks :process

    def self.process(update)
      new(update).send(:call)
    end

    attr_reader :update

    def initialize(update)
      @update = update
    end

    def process
      puts '- Processing'
    end

    private

    def bot
      "#{self.class.name.deconstantize}::App".constantize.bot
    end

    def call
      run_callbacks :process do
        process
      end
    rescue Exception => exception
      rescue_with_handler(exception) || raise(exception)
    end

    def send_message(message_class, opts: {}, with: bot, to: update.chat.id)
      Helpers.send_message(message_class, opts: opts, with: with, to: to)
    end

    def edit_message(message_class, opts: {}, with: bot, to: update.callback_query.message.chat.id, message_id: update.callback_query.message.message_id)
      Helpers.edit_message(message_class, opts: opts, with: with, to: to, message_id: message_id)
    end
  end
end
