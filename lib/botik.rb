require 'telegram/bot'
require 'active_support/rescuable'
require 'active_support/callbacks'
require 'active_support/core_ext/string/inflections'

require 'botik/version'
require 'botik/telegram_bot_ext'
require 'botik/configuration'
require 'botik/helpers'
require 'botik/app'
require 'botik/controller'
require 'botik/message'
require 'botik/edit_message'

module Botik
end
